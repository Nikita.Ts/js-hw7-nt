// Option №1

function filterBy(array, type) {
    return array.reduce(function(res, currentItem) {
        if (typeof currentItem != type) {
            res.push(currentItem)
        }
        return res
    }, [])
}
console.log(filterBy(['hello', 'world', 23, '23', null], 'string')) 



// Option №2

// let testArr = ['hello', 'world', 23, '23', null]
// function fliterByTest(arr, type) {
//     return arr.filter(function(item) {
//         return typeof item !== type
//     })
// }
// console.log(fliterByTest(testArr, 'string')) 